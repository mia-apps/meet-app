module gitlab.com/mia-apps/meet-app

go 1.15

require (
	github.com/gin-contrib/logger v0.0.2
	github.com/gin-gonic/gin v1.6.3
	github.com/gookit/config/v2 v2.0.21
	github.com/prometheus/client_golang v1.9.0
	github.com/rs/zerolog v1.20.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/pflag v1.0.3
	go.mongodb.org/mongo-driver v1.4.6
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e
)
