<html>
<head>
<title>{{ .title }}</title>
</head>
<body>
<div style="width: 400px; margin: auto">
<p align="center" style="font-size: 20px">{{ .item.Name }}</p>
<div class="text" align="center" style="font-size: 15px">
    <pre>{{ .item.Text }}</pre>
</div>
<div class="text" align="right" style="font-size: 14px">
    <pre>{{ .item.Author }}</pre>
</div>
<div>
</body>
</html> 