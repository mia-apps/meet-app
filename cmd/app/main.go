package main

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gookit/config/v2"
	"github.com/gookit/config/v2/yaml"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
	flag "github.com/spf13/pflag"
	"golang.org/x/sync/errgroup"

	"gitlab.com/mia-apps/meet-app/pkg/utils"
	"gitlab.com/mia-apps/meet-app/pkg/utils/env"
	"gitlab.com/mia-apps/meet-app/pkg/web/handler"
	"gitlab.com/mia-apps/meet-app/pkg/web/metrics"
	"gitlab.com/mia-apps/meet-app/pkg/web/mongo"
	"gitlab.com/mia-apps/meet-app/pkg/web/types"
)

const (
	version = ""
	appName = "meet-app"
)

const (
	generalConfig   = "config/general.yaml"
	overrideConfig  = "config/override.yaml"
	migrationConfig = "config/migration.yaml"
	templateGlob    = "templates/*"
)

const (
	dfltMetricsURL  = "/metrics"
	dfltMetricsPort = 8080
	dfltAppURL      = "/"
	dfltAppPort     = 8000
)

const (
	envMongoHost     = "MONGO_HOST"
	envAppPort       = "APP_PORT"
	envMetricsPort   = "METRICS_PORT"
	flagMongoHost    = "mongohost"
	flagMongoPort    = "mongoport"
	flagMongoMigrate = "migrate"
	flagMemLeak      = "memoryleak"
	flagAppPort      = "aport"
	flagMetricsPort  = "mport"
)

var (
	mongoHost    string
	mongoPort    int
	mongoMigrate bool
	memLeak      bool
	appPort      int
	metricsPort  int
	withMongo    handler.Option = handler.NopOption
	withMemLeak  handler.Option = handler.NopOption
)

var (
	rootCmd = &cobra.Command{
		Use:     appName,
		Version: version,
		PreRun: func(cmd *cobra.Command, args []string) {
			rootCmdPreRun(cmd, args)
		},
		RunE: func(cmd *cobra.Command, args []string) error {
			return rootCmdRun()
		},
		SilenceUsage: true,
	}
)

func init() {
	rootCmd.PersistentFlags().StringVar(
		&mongoHost, flagMongoHost, "",
		"mongodb server hostname or ip address",
	)
	rootCmd.PersistentFlags().IntVar(
		&mongoPort, flagMongoPort, 27017,
		"mongodb server port",
	)
	rootCmd.PersistentFlags().BoolVar(
		&mongoMigrate, flagMongoMigrate, false,
		"mongodb migrate (populate data)",
	)
	rootCmd.PersistentFlags().BoolVar(
		&memLeak, flagMemLeak, false,
		"enable memory leak",
	)
	rootCmd.PersistentFlags().IntVar(
		&appPort, flagAppPort, dfltAppPort,
		"application port",
	)
	rootCmd.PersistentFlags().IntVar(
		&metricsPort, flagMetricsPort, dfltMetricsPort,
		"metrics port",
	)
}

func rootCmdPreRun(cmd *cobra.Command, args []string) {
	cmd.PersistentFlags().VisitAll(func(f *flag.Flag) {
		switch f.Name {
		case flagMongoHost:
			mongoHost = env.GetEnvDefault(envMongoHost, mongoHost)
			if mongoHost != "" {
				withMongo = handler.WithMongoURI(
					fmt.Sprintf("mongodb://%s:%d", mongoHost, mongoPort),
				)
			}
		case flagMemLeak:
			if memLeak {
				withMemLeak = handler.WithMemLeak()
			}
		case flagAppPort:
			appPort = env.GetEnvDefaultInt(envAppPort, appPort)
		case flagMetricsPort:
			metricsPort = env.GetEnvDefaultInt(envMetricsPort, metricsPort)
		}
	})
}

func cmdMigrate() error {
	cfg := config.NewEmpty(appName)
	cfg.AddDriver(yaml.Driver)
	err := cfg.LoadFiles(migrationConfig)
	if err != nil {
		return err
	}
	db := mongo.New(
		mongo.WithURI(fmt.Sprintf("mongodb://%s:%d", mongoHost, mongoPort)),
	)
	if err := utils.RunTimesDelay(
		10, time.Second*5,
		func() error {
			return db.Populate(cfg)
		},
	); err != nil {
		return err
	}
	return nil
}

func rootCmdRun() error {
	if mongoMigrate {
		return cmdMigrate()
	}
	cfg := config.NewEmpty(appName)
	cfg.AddDriver(yaml.Driver)

	err := cfg.LoadFiles(generalConfig, overrideConfig)
	if err != nil {
		return err
	}
	zerolog.SetGlobalLevel(zerolog.InfoLevel)
	logger := log.Output(
		zerolog.ConsoleWriter{
			Out:        os.Stderr,
			NoColor:    false,
			TimeFormat: time.RFC3339,
		},
	)

	counter := metrics.New(
		metrics.WithLogger(logger),
	)
	router := types.New(
		types.WithMetrics(counter),
		types.WithLogger(logger),
		types.WithHTMLGlob(templateGlob),
		types.WithRecovery(),
	)
	poems := handler.New(
		handler.InPoemsMode(),
		handler.WithConfig(cfg),
		handler.WithMetrics(counter),
		withMongo,
		withMemLeak,
	)
	haiku := handler.New(
		handler.InHaikuMode(),
		handler.WithConfig(cfg),
		handler.WithMetrics(counter),
		withMemLeak,
	)
	generalRoute := router.Register(poems)
	generalRoute.Route("/haiku").Register(haiku).Route("/poems").Register(poems)
	generalRoute.Route("/poems").Register(poems).Route("/haiku").Register(haiku)
	generalRoute.Route("/healty").Register(handler.HealthyHandler)

	wrapHandler := func(prefix string, h http.Handler) http.Handler {
		mux := http.NewServeMux()
		mux.Handle(prefix, h)
		return mux
	}

	srvMetrics := &http.Server{
		Addr:    fmt.Sprintf(":%d", metricsPort),
		Handler: wrapHandler(dfltMetricsURL, promhttp.Handler()),
	}

	srvApp := &http.Server{
		Addr:    fmt.Sprintf(":%d", appPort),
		Handler: wrapHandler(dfltAppURL, router),
	}

	wg, ctxWg := errgroup.WithContext(context.Background())

	sigMetrics := make(chan os.Signal)
	sigApp := make(chan os.Signal)
	signal.Notify(sigMetrics, syscall.SIGINT, syscall.SIGTERM)
	signal.Notify(sigApp, syscall.SIGINT, syscall.SIGTERM)
	defer close(sigMetrics)
	defer close(sigApp)

	wg.Go(func() error {
		logger.Info().Int("port", metricsPort).Str("url", dfltMetricsURL).Msg("Metrics listener started...")
		if err := srvMetrics.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			logger.Info().Str("reason", err.Error()).Msg("Metrics listener")
			return nil
		} else if err != nil {
			return err
		}
		return nil
	})

	wg.Go(func() error {
		logger.Info().Int("port", appPort).Str("url", dfltAppURL).Msg("App listener started...")
		if err := srvApp.ListenAndServe(); err != nil && errors.Is(err, http.ErrServerClosed) {
			logger.Info().Str("reason", err.Error()).Msg("App listener")
			return nil
		} else if err != nil {
			return err
		}
		return nil
	})

	wg.Go(func() error {
		shutdown := func(ctx context.Context) error {
			if err := srvMetrics.Shutdown(ctx); err != nil {
				logger.Err(err).Msg("Metrics server forced to shutdown")
				return err
			}
			return nil
		}
		select {
		case <-ctxWg.Done():
			return shutdown(ctxWg)
		case sig := <-sigMetrics:
			logger.Info().Msgf("Metrics signal \"%s\" received", sig)
			return shutdown(ctxWg)
		}
	})

	wg.Go(func() error {
		shutdown := func(ctx context.Context) error {
			if err := srvApp.Shutdown(ctxWg); err != nil {
				logger.Err(err).Msg("App server forced to shutdown")
				return err
			}
			return nil
		}
		select {
		case <-ctxWg.Done():
			return shutdown(ctxWg)
		case sig := <-sigApp:
			logger.Info().Msgf("App signal \"%s\" received", sig)
			return shutdown(ctxWg)
		}
	})

	return wg.Wait()
}

func main() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
