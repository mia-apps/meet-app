package env

import (
	"os"
	"strconv"
)

// GetEnvDefault ...
func GetEnvDefault(name, defaultValue string) string {
	if value, ok := os.LookupEnv(name); ok {
		return value
	}
	return defaultValue
}

// GetEnvDefaultInt ...
func GetEnvDefaultInt(name string, defaultValue int) int {
	if value, ok := os.LookupEnv(name); ok {
		intValue, err := strconv.ParseInt(value, 10, 0)
		if err != nil {
			panic(err)
		}
		return int(intValue)

	}
	return defaultValue
}
