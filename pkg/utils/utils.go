package utils

import (
	"time"
)

func RunTimesDelay(times int, delay time.Duration, f func() error) (err error) {
	for i := 0; i < times; i++ {
		err = f()
		if err == nil {
			return nil
		}
		time.Sleep(delay)
	}
	return
}
