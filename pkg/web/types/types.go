package types

import (
	"errors"

	"github.com/gin-contrib/logger"
	"github.com/gin-gonic/gin"
	"github.com/gookit/config/v2"
	"github.com/rs/zerolog"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type router struct {
	*gin.RouterGroup
}

func (r *router) Route(relPath string) Router {
	return &router{
		RouterGroup: r.RouterGroup.Group(relPath),
	}
}

func (r *router) Register(h Handler) Router {
	h.Register(r)
	return r
}

func (r *router) Group() *gin.RouterGroup {
	return r.RouterGroup
}

// Router ...
type Router interface {
	Route(relPath string) Router
	Register(h Handler) Router
	Group() *gin.RouterGroup
}

// Handler ...
type Handler interface {
	Register(Router)
}

// Engine ...
type Engine struct {
	Router
	*gin.Engine
}

// Option ...
type Option func(e *Engine)

// WithRecovery ...
func WithRecovery() Option {
	return func(e *Engine) {
		e.Engine.Use(gin.Recovery())
	}
}

// WithMetrics ...
func WithMetrics(m Metrics) Option {
	return func(e *Engine) {
		e.Engine.Use(func(c *gin.Context) {
			c.Next()
			if c.Writer.Status() >= 300 {
				m.IncBad(
					c.Request.URL.Path,
					c.Request.URL.RawQuery,
					c.Request.UserAgent(),
					c.Writer.Status(),
				)
			}
		})
	}
}

// WithLogger ...
func WithLogger(l zerolog.Logger) Option {
	return func(e *Engine) {
		if gin.IsDebugging() {
			l.Level(zerolog.DebugLevel)
		} else {
			l.Level(zerolog.InfoLevel)
		}
		e.Engine.Use(
			logger.SetLogger(
				logger.Config{Logger: &l},
			),
		)
	}
}

// WithHTMLGlob ...
func WithHTMLGlob(pattern string) Option {
	return func(e *Engine) {
		e.Engine.LoadHTMLGlob(pattern)
	}
}

// New ...
func New(opts ...Option) *Engine {
	e := &Engine{
		Engine: gin.New(),
	}
	e.Router = &router{
		RouterGroup: &e.Engine.RouterGroup,
	}
	for _, o := range opts {
		o(e)
	}
	return e
}

// ItemType ...
type ItemType int

func (t ItemType) String() string {
	switch t {
	case TypePoems:
		return "poems"
	case TypeHaiku:
		return "haiku"
	default:
		panic(errors.New("Unknown type"))
	}
}

const (
	// TypePoems ...
	TypePoems ItemType = iota
	// TypeHaiku ...
	TypeHaiku
)

// Item ...
type Item struct {
	ID     primitive.ObjectID `bson:"_id,omitempty"`
	Author string             `json:"author" bson:"author"`
	Name   string             `json:"name,omitempty" bson:"name,omitempty"`
	Text   string             `json:"text" bson:"text"`
}

// Items ...
type Items []Item

// Metrics ...
type Metrics interface {
	Inc(author, typ, url, raw, agent string, code int)
	IncBad(url, raw, agent string, code int)
}

// DB ...
type DB interface {
	GetItem(ItemType) (Item, error)
	Populate(cfg *config.Config) error
}

// MemLeak ...
type MemLeak interface {
	Go() error
}
