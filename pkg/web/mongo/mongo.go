package mongo

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/mia-apps/meet-app/pkg/web/types"
	"github.com/gookit/config/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

const (
	dbURI  = "mongodb://localhost:27017"
	dbName = "meetapp"
)

type db struct {
	uri    string
	dbName string
}

func (d *db) Connect(ctx context.Context) (*mongo.Client, error) {
	return mongo.Connect(ctx, options.Client().ApplyURI(d.uri))
}

func (d *db) Disconnect(ctx context.Context, client *mongo.Client) error {
	return client.Disconnect(ctx)
}

func (d *db) Ping(ctx context.Context, client *mongo.Client, timeout time.Duration) error {
	pingCtx, pingCancel := context.WithTimeout(ctx, timeout)
	defer pingCancel()
	return client.Ping(pingCtx, readpref.Primary())
}

func (d *db) GetCollection(ctx context.Context, client *mongo.Client, colName string) (*mongo.Collection, error) {
	colNames, err := client.Database(d.dbName).ListCollectionNames(ctx, bson.D{})
	if err != nil {
		return nil, err
	}
	for _, name := range colNames {
		if name == colName {
			return client.Database(d.dbName).Collection(colName), nil
		}
	}
	if err := client.Database(d.dbName).CreateCollection(ctx, colName); err != nil {
		return nil, err
	}
	return client.Database(d.dbName).Collection(colName), nil
}

func (d *db) GetRandomItem(ctx context.Context, col *mongo.Collection) (item types.Item, err error) {
	sample := bson.D{{
		Key: "$sample",
		Value: bson.D{{
			Key:   "size",
			Value: 1,
		}},
	}}
	cur, err := col.Aggregate(ctx, mongo.Pipeline{sample})
	if err != nil {
		return item, err
	}
	for cur.Next(ctx) {
		if err := cur.Decode(&item); err != nil {
			return item, err
		}
	}
	if err := cur.Err(); err != nil {
		return item, err
	}
	return
}

func (d *db) GetItem(t types.ItemType) (item types.Item, err error) {
	ctx := context.Background()
	client, err := d.Connect(ctx)
	if err != nil {
		return item, err
	}
	defer func() {
		if err := d.Disconnect(ctx, client); err != nil {
			panic(err)
		}
	}()
	if err := d.Ping(ctx, client, time.Second*2); err != nil {
		return item, err
	}
	col, err := d.GetCollection(ctx, client, t.String())
	if err != nil {
		return item, err
	}
	return d.GetRandomItem(ctx, col)
}

func (d *db) Populate(cfg *config.Config) error {
	ctx := context.Background()
	client, err := d.Connect(ctx)
	if err != nil {
		return err
	}
	defer func() {
		if err := d.Disconnect(ctx, client); err != nil {
			panic(err)
		}
	}()
	if err := d.Ping(ctx, client, time.Second*2); err != nil {
		return fmt.Errorf("mongodb ping error: %w", err)
	}
	inserter := func(t types.ItemType) error {
		items := types.Items{}
		err := cfg.MapStruct(t.String(), &items)
		if err != nil {
			return err
		}
		col, err := d.GetCollection(ctx, client, t.String())
		if err != nil {
			return err
		}
		insertItems := []interface{}{}
		for _, item := range items {
			insertItems = append(insertItems, item)
		}
		if _, err := col.InsertMany(ctx, insertItems); err != nil {
			return err
		}
		return nil
	}
	for _, t := range []types.ItemType{types.TypePoems, types.TypeHaiku} {
		if err := inserter(t); err != nil {
			return err
		}
	}
	return nil
}

// New ...
func New(opts ...Option) types.DB {
	db := &db{
		uri:    dbURI,
		dbName: dbName,
	}
	for _, o := range opts {
		o(db)
	}
	return db
}
