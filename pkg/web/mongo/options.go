package mongo

// Option ...
type Option func(d *db)

// WithURI ...
func WithURI(uri string) Option {
	return func(d *db) {
		d.uri = uri
	}
}

// WithDB ...
func WithDB(name string) Option {
	return func(d *db) {
		d.dbName = name
	}
}
