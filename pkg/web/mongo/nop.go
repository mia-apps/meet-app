package mongo

import (
	"gitlab.com/mia-apps/meet-app/pkg/web/types"
	"github.com/gookit/config/v2"
)

type nop struct{}

func (*nop) GetItem(types.ItemType) (item types.Item, err error) { return }
func (*nop) Populate(cfg *config.Config) error                   { return nil }

// Nop ...
func Nop() types.DB {
	return &nop{}
}
