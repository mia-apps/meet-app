package metrics

import "github.com/rs/zerolog"

// Option ...
type Option func(m *metrics)

// WithLogger ...
func WithLogger(l zerolog.Logger) Option {
	return func(m *metrics) {
		m.logger = l
	}
}
