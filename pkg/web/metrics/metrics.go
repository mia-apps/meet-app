package metrics

import (
	"strconv"

	"gitlab.com/mia-apps/meet-app/pkg/web/types"
	"github.com/prometheus/client_golang/prometheus"

	"github.com/rs/zerolog"
)

const (
	labelAuthor    = "author"
	labelType      = "type"
	labelURL       = "urlpath"
	labelCode      = "code"
	labelUserAgent = "useragent"
	labelRaw       = "rawquery"
)

type metrics struct {
	logger     zerolog.Logger
	counter    *prometheus.CounterVec
	badCounter *prometheus.CounterVec
}

// New ...
func New(opts ...Option) types.Metrics {
	counter := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "meet_app_requests",
			Help: "How many http requests processed",
		},
		[]string{labelAuthor, labelType, labelURL, labelRaw, labelUserAgent, labelCode},
	)
	badCounter := prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "meet_app_bad_requests",
			Help: "How many not 200 http requests processed",
		},
		[]string{labelURL, labelRaw, labelUserAgent, labelCode},
	)
	prometheus.MustRegister(counter)
	m := &metrics{
		logger:     zerolog.Nop(),
		counter:    counter,
		badCounter: badCounter,
	}
	for _, o := range opts {
		o(m)
	}
	return m
}

func (m *metrics) IncBad(url, raw, agent string, code int) {
	m.badCounter.WithLabelValues(url, raw, agent, strconv.Itoa(code)).Inc()
	m.logger.Warn().
		Str("url", url).
		Str("raw", raw).
		Str("agent", agent).
		Int("code", code).
		Msg("Metric")
}

func (m *metrics) Inc(author, typ, url, raw, agent string, code int) {
	m.counter.WithLabelValues(author, typ, url, raw, agent, strconv.Itoa(code)).Inc()
	m.logger.Info().
		Str("author", author).
		Str("type", typ).
		Str("url", url).
		Str("raw", raw).
		Str("agent", agent).
		Int("code", code).
		Msg("Metric")
}
