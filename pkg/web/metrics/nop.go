package metrics

import "gitlab.com/mia-apps/meet-app/pkg/web/types"

type nop struct{}

func (*nop) Inc(author, typ, url, raw, agent string, code int) {}
func (*nop) IncBad(url, raw, agent string, code int)           {}
func (*nop) ListenAndServe(addr string)                        {}

// Nop ...
func Nop() types.Metrics {
	return &nop{}
}
