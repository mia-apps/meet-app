package handler

import (
	"gitlab.com/mia-apps/meet-app/pkg/web/mongo"
	"gitlab.com/mia-apps/meet-app/pkg/web/types"

	"github.com/gookit/config/v2"
)

var (
	// NopOption ...
	NopOption Option = func(h *handler) {}
)

// Option ...
type Option func(h *handler)

// WithTplFile ...
func WithTplFile(file string) Option {
	return func(h *handler) {
		h.tplFile = file
	}
}

// WithMongoURI ...
func WithMongoURI(uri string) Option {
	return func(h *handler) {
		h.mongoURI = &uri
		h.db = mongo.New(
			mongo.WithURI(uri),
		)
	}
}

// WithConfig ...
func WithConfig(cfg *config.Config) Option {
	return func(h *handler) {
		h.config = cfg
	}
}

// InHaikuMode ...
func InHaikuMode() Option {
	return func(h *handler) {
		h.itemType = types.TypeHaiku
	}
}

// InPoemsMode ...
func InPoemsMode() Option {
	return func(h *handler) {
		h.itemType = types.TypePoems
	}
}

// WithMetrics ...
func WithMetrics(m types.Metrics) Option {
	return func(h *handler) {
		h.metrics = m
	}
}

// WithMemLeak ...
func WithMemLeak() Option {
	return func(h *handler) {
		h.memLeak = true
	}
}
