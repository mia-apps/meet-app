package handler

import (
	"errors"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"gitlab.com/mia-apps/meet-app/pkg/web/metrics"
	"gitlab.com/mia-apps/meet-app/pkg/web/mongo"
	"gitlab.com/mia-apps/meet-app/pkg/web/types"

	"github.com/gin-gonic/gin"
	"github.com/gookit/config/v2"
)

const (
	tplFile = "index.tpl"
)

type handler struct {
	tplFile  string
	mongoURI *string
	config   *config.Config
	itemType types.ItemType
	metrics  types.Metrics
	db       types.DB
	memLeak  bool
}

func (h *handler) getItemFromMongo() (types.Item, error) {
	return h.db.GetItem(h.itemType)
}

func (h *handler) getItemFromConfigData() (item types.Item, err error) {
	items := types.Items{}
	err = h.config.MapStruct(h.itemType.String(), &items)
	if err != nil {
		return
	}
	item = h.getRandomItem(items)
	return
}

func (h *handler) getRandomItem(items types.Items) types.Item {
	if len(items) > 0 {
		rand.Seed(time.Now().Unix())
		n := rand.Int() % len(items)
		return items[n]
	}
	return types.Item{}
}

func (h *handler) getItem() (types.Item, error) {
	if h.mongoURI != nil {
		return h.getItemFromMongo()
	}
	if h.config != nil {
		return h.getItemFromConfigData()
	}
	return types.Item{}, errors.New("mongoURI and config attrs is nil")
}

func (h *handler) Register(r types.Router) {
	r.Group().Handle("GET", "/", func(c *gin.Context) {
		if h.memLeak {
			go NewLeak().Go()
		}
		item, err := h.getItem()
		if err != nil {
			panic(err)
		}
		c.HTML(http.StatusOK, h.tplFile, gin.H{
			"title": fmt.Sprintf("Item type \"%s\"", h.itemType),
			"item":  item,
		})
		h.metrics.Inc(
			item.Author,
			h.itemType.String(),
			c.FullPath(),
			c.Request.URL.RawQuery,
			c.Request.UserAgent(),
			c.Writer.Status(),
		)
	})
}

// New ...
func New(opts ...Option) types.Handler {
	h := &handler{
		tplFile: tplFile,
		metrics: metrics.Nop(),
		db:      mongo.Nop(),
	}
	for _, o := range opts {
		o(h)
	}
	return h
}
