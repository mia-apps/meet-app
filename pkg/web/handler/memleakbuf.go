package handler

import (
	"crypto/rand"
	"io"

	"gitlab.com/mia-apps/meet-app/pkg/web/types"
)

type writer struct {
	data []byte
}

func (w *writer) Write(p []byte) (n int, err error) {
	w.data = append(w.data, p...)
	return len(p), nil
}

func (w *writer) Go() error {
	_, err := io.Copy(w, rand.Reader)
	return err
}

// NewLeak ...
func NewLeak() types.MemLeak {
	return &writer{
		data: []byte{},
	}
}
