package handler

import (
	"net/http"

	"gitlab.com/mia-apps/meet-app/pkg/web/types"
	"github.com/gin-gonic/gin"
)

const (
	unknownUserAgent = "unknown"
	statusOK         = "ok"
)

type Healthy func(c *gin.Context)

func (f Healthy) Register(r types.Router) {
	r.Group().Handle("GET", "/", gin.HandlerFunc(f))
}

func getUserAgent(c *gin.Context) (agent string) {
	agent = c.Request.Header.Get("User-Agent")
	if agent == "" {
		return unknownUserAgent
	}
	return
}

var HealthyHandler Healthy = func(c *gin.Context) {
	c.JSON(http.StatusOK, struct {
		Status    string `json:"status"`
		UserAgent string `json:"user-agent"`
	}{
		statusOK,
		getUserAgent(c),
	})
}
