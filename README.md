# kubectl (https://kubernetes.io/ru/docs/reference/kubectl/overview/)
 * установка (https://kubernetes.io/ru/docs/tasks/tools/install-kubectl/) 
```
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.0/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl
sudo install kubectl /usr/local/bin/
```  
# helm3 (https://helm.sh/)
 * установка 
```
 curl -Lo helm.tar.gz https://get.helm.sh/helm-v3.7.2-linux-amd64.tar.gz && \
    tar -xpzvf helm.tar.gz && \
    chmod +x linux-amd64/helm
 sudo install linux-amd64/helm /usr/local/bin/
 ```
# stern 
 * установка
```
curl -Lo stern https://github.com/wercker/stern/releases/download/1.11.0/stern_linux_amd64 && \
  chmod +x ./stern
sudo install ./stern /usr/local/bin
```

# minikube (k8s)
 * установка https://kubernetes.io/ru/docs/tasks/tools/install-minikube/
```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64 && \
    chmod +x ./minikube
sudo mkdir -p /usr/local/bin/
sudo install minikube /usr/local/bin/
```
 * запуск
```
minikube start --driver=virtualbox --memory=4g
```
  

# gitlab runner (что такое в контексте gitlab и зачем надо)
  * регистрация раннера и запуск (https://docs.gitlab.com/runner/register/) в docker 

```
mkdir -p /tmp/gitlab-runner/config 
docker run --rm -v /tmp/gitlab-runner/config:/etc/gitlab-runner gitlab/gitlab-runner register \
--non-interactive \
--executor "docker" \
--docker-image docker:20.10 \
--url "https://gitlab.com/" \
--registration-token "PROJECT_REGISTRATION_TOKEN" \
--description "docker runner for mia-apps group (BelGTU)" \
--maintenance-note "Free-form maintainer notes about this runner" \
--tag-list "selfhosted" \
--run-untagged="false" \
--locked="false" \
--access-level="not_protected"
```

```
cat /tmp/gitlab-runner/config/config.toml
```

  ```
  concurrent = 1
  check_interval = 0

  [session_server]
    session_timeout = 1800

  [[runners]]
    name = "docker runner for mia-apps group (BelGTU)"
    url = "https://gitlab.com/"
    token = "PROJECT_ACCESS_TOKEN"
    executor = "docker"
    [runners.custom_build_dir]
    [runners.cache]
      [runners.cache.s3]
      [runners.cache.gcs]
      [runners.cache.azure]
    [runners.docker]
      tls_verify = false
      image = "docker:20.10"
      privileged = false
      disable_entrypoint_overwrite = false
      oom_kill_disable = false
      disable_cache = false
      volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
      shm_size = 0
  ```

   пример запуска в docker-swarm (compose file):

  ```
  version: '3.7'

  x-settings:
    image: &img gitlab/gitlab-runner:v13.11.0
    default_volumes: &default_volumes
      volumes:
        - /var/run/docker.sock:/var/run/docker.sock
    constraints: &constraints
      constraints:
        - node.platform.os == linux
    deploy: &deploy
      deploy:
        placement:
          <<: *constraints
        mode: replicated
        replicas: 1
  services:
    runner_mia_apps_group_0:
      image: *img
      <<: *default_volumes
      <<: *deploy
      configs:
      - source: runner_mia_apps_group_0
        target: /etc/gitlab-runner/config.toml
  configs:
    runner_mia_apps_group_0:
      name: runner_mia_apps_group_0_v1
      file: ./runner_mia_apps_group_0.toml
  ```


  * dind, docker socket, kaniko и т.д. 
  * деплой раннера в minikube (k8s)
  
  ```
  # если кластер не один, нужно переключиться на нужный
  kubectl config use-context minikube
  # создаем namespace
  kubectl create ns gitlab
  # создаем serviceaccount и, как следствие, secret/token
  kubectl -n gitlab create sa gitlab-runner
  # "биндим" к нему админускую роль 
  kubectl create clusterrolebinding gitlab-runner \
    --clusterrole=cluster-admin \
    --serviceaccount=gitlab:gitlab-runner
  helm repo add gitlab https://charts.gitlab.io
  helm upgrade --install \
    --namespace gitlab \
    --version 0.38.1 mia-apps gitlab/gitlab-runner \
    --set rbac.serviceAccountName=gitlab-runner \
    --set runners.serviceAccountName=gitlab-runner \
    --set runners.tags="selfhosted-k8s" \
    --set runnerRegistrationToken=PROJECT_REGISTRATION_TOKEN \
    --set gitlabUrl=https://gitlab.com/
  ```

# Helm (что такое и зачем)
  * helm chart
  * helm registry
  * проблемы есть, можно поговорить

# CI/CD tools
   * helmfile (https://github.com/roboll/helmfile) 
   * fluxcd (https://fluxcd.io/docs/, https://github.com/fluxcd/flux2)
   * argocd (https://argo-cd.readthedocs.io/en/stable/)
   * ...
# Логирование 
  * обзор инструментов и подходов (упомянуть про сайдкары)
    * loki
    * elk (efk)
    * ...
  * пример использования kubectl
  ```
  kubectl -n meet-app logs --since=1h --selector=name=meet-app -f
  ```
  * пример использования stern
  ```
  stern -n meet-app meet-app
  ```

# Метрики
  * prometheus
  * statsd
  * tick стэк (https://www.8host.com/blog/monitoring-sistemnyx-metrik-s-pomoshhyu-steka-tick-v-centos-7/)