FROM golang:1.17-buster as build

RUN mkdir /opt/build && go env

WORKDIR /opt/build

COPY . ./

RUN make build

FROM scratch

ENV WORKDIR=/usr/local/bin/

COPY --from=build /opt/build/meet-app ${WORKDIR}

COPY --from=build /opt/build/templates/* ${WORKDIR}/templates/

COPY --from=build /opt/build/config/* ${WORKDIR}/config/

WORKDIR ${WORKDIR}

ENTRYPOINT ["/usr/local/bin/meet-app"]
