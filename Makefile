GO := CGO_ENABLED=0 go
IMAGE_NAME := registry.gitlab.com/mia-apps/meet-app
IMAGE_TAG := 1.0.6

.PHONY: build
build:
	$(GO) build -ldflags "-X main.version=${IMAGE_TAG}" -o meet-app ./cmd/app

.PHONY: build-image
build-image:
	docker build -t ${IMAGE_NAME}:${IMAGE_TAG} .

.PHONY: push-image
push-image:
	docker push ${IMAGE_NAME}:${IMAGE_TAG}

.PHONY: docker
docker: build-image push-image

.PHONY: install-mongo
install-mongo: 
	helm upgrade --install mongodb --set auth.enabled=false bitnami/mongodb

.PHONY: unshare
.ONESHELL:
unshare:

	unshare --map-root-user --pid --mount-proc --fork ./meet-app
	echo $1
		
